'use strict';

(function () {
  // Валидация поля для комментариев
  /* строка длинее 140 символов 12345678901234567890123456789012345678901234567890123456789012
   345678901234567890123456789012345678901234567890123456789012345678901234567890
    */
  const textDescription = window.preview.picturesContainer.querySelector('.text__description');

  textDescription.addEventListener('blur', function (evt) {
    if (evt.target.validity.tooLong) {
      evt.target.setCustomValidity('Длина сообщения не должна превышать 140 символов');
    }
    // для браузеров на движке Gecko
    if (evt.target.value.length > 139) {
      evt.target.setCustomValidity('Длина сообщения не должна превышать 140 символов');
    }
  })

  // Валидация хеш-тегов

  // const imgUploadForm = document.querySelector('#upload-select-image'); // форма редактирования
  const hashtag = window.form.upload.querySelector('.text__hashtags'); // поле ввода хэштегов

  function hashtagSplit(str, separator) {
    return str.split(separator);
  }

  const splittingRegexp = ' ';
  // тэги #first #second #third #fourth #fifth #sixth # #@ seventh
  // обработчик потери фокуса
  const forbiddenSymbols = ['!', '@', '$', '%', '^', '&', '*', '(', ')', '-', '=', '\\', '[', ']', ';', '\'', ',', '.', '/', '+', '|', '{', '}', ':', '"', '<', '>', '?', '`', '~'];
  const warnings = [
    ' - запрещенный символ. В имени тэга могут быть только буквы и цифры',
    'Тэг должен начинаться с "#"',
    'Тэгов не может быть больше 5',
    'Длина тэга не может превышать 20 символов',
    'Этот тэг уже был введен']

  const onHashtagBlur = function (evt) {

    let symbols = evt.target.value;
    let enteredCharacter = symbols[symbols.length - 1];
    let tags = hashtagSplit(symbols, splittingRegexp);

    if (forbiddenSymbols.includes(enteredCharacter)) {
      evt.target.setCustomValidity(`"${enteredCharacter}"${warnings[0]}`);
    }

    if (tags[tags.length - 1][0] !== '#' && tags[tags.length - 1][0] !== undefined) {
      evt.target.setCustomValidity(warnings[1]);
    }

    if (tags.length > 5) {
      evt.target.setCustomValidity(warnings[2]);
    }

    for (let i = 0; i < tags.length; i++) {
      if (tags[i].length > 20) {
        evt.target.setCustomValidity(warnings[3]);
      }
    }

    if (tags.length >= 2) {
      for (let j = 1; j < tags.length; j++) {
        let popped = tags.pop();
        if (tags.includes(popped)) {
          evt.target.setCustomValidity(warnings[4]);
        }
      }
    }
  }

  // hashtag.addEventListener('blur', onHashtagBlur); // регистрирую обработчик на событии потери фокуса
  hashtag.addEventListener('input', onHashtagBlur); //
})()

// ФОРМУЛИРОВКА ЗАДАЧИ
/* 1. Из введенных в поле hashtag значений создать 2 массива
  2. Первый -- массив всех введенных символов. Назовем его symbols
  3. Второй -- массив из отдельных хэштегов. Назовем его tags
  4. Пройти по массиву symbols и при обнаружении запрещенных символов создать сообщение
  через customValidity selectElt.setCustomValidity(string);
  5. Пройти по массиву tags и создать сообщение через customValidity при обнаружении
  невалидных значений
 */
