'use strict';

(function () {
  // функция создания моки
  function getMoke(amount) {
    let mokes = [];

    for (let i = 0; i < amount; i++) {
      mokes[i] = {
        url: 'photos/' + (i + 1) + '.jpg',
        description: window.data.descriptions[i],
        likes: window.data.randomInteger(15, 200),
        comments:
        {
          count: window.data.random(200),
          name: window.data.nicknames[window.data.random(9)],
          avatar: 'img/avatar-' + window.data.randomInteger(1, 6) + '.svg',
          message: window.data.messages[window.data.random(window.data.numOfComments - 1)], // случайное сообщение из шести
        },
      }
    }
    return mokes;
  }

  // запуск функции создания моки
  const moke = getMoke(window.data.amountOfObj);

  // функция создания нового массива индексов старого массива
  function getNewArrOfIndexes(oldArr) {
    let arr = [];
    for (let i = 0; i < oldArr.length; i++) {
      arr.push(i);
    }
    return arr;
  }
  const sequenceNumbers = getNewArrOfIndexes(moke);

  // функция выбора случайных уникальных count элементов из массива arr. Синтаксис ES6
  const pickRandom = (arr, count) => {
    let _arr = [...arr];
    return [...Array(count)].map(() => _arr.splice(Math.floor(Math.random() * _arr.length), 1)[0]);
  }

  const newIndexArr = pickRandom(sequenceNumbers, 6); // массив случайных шести индексов из 25

  window.auxData = {
    moke: moke,
    newIndexArr: newIndexArr,
  }

})()

