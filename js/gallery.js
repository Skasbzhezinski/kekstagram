'use strict';

(function () {
  // функция показа фотографий на титульной странице
  const showPhotos = function () {
    let fragment = document.createDocumentFragment();
    for (let i = 0; i < window.data.numOfPictures; i++) {
      window.data.randomImg.src = window.auxData.moke[window.auxData.newIndexArr[i]].url;
      window.data.pictureComments.textContent = window.auxData.moke[window.auxData.newIndexArr[i]].comments.message;
      window.data.pictureLikes.textContent = window.auxData.moke[window.auxData.newIndexArr[i]].likes;
      let newImage = window.data.randomPictureTemplate.cloneNode(true);
      fragment.appendChild(newImage);
    }
    window.data.imagesBlock.appendChild(fragment);
  }

  showPhotos(); // запуск функции показа фотографий
})()
