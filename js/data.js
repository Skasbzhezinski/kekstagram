'use strict';

(function () {

  const imagesBlock = document.querySelector('.pictures');
  const randomPictureTemplate = document.querySelector('#picture').content;
  const randomImg = randomPictureTemplate.querySelector('.picture__img');
  const pictureComments = randomPictureTemplate.querySelector('.picture__comments');
  const pictureLikes = randomPictureTemplate.querySelector('.picture__likes');

  const numOfComments = 8; // количество комментов в моки
  const numOfPictures = 6; // кол-во фотографий на титульной странице
  const amountOfObj = 25; // кол-во объектов с данными

  // вспомогательные функции
  // функция нахождения случайного числа в диапазоне
  function randomInteger(min, max) {
    // случайное число от min до (max+1)
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
  }

  // вызов случайного числа от 0 до number
  function random(number) {
    return Math.floor(Math.random() * (number + 1));
  }

  // массивы данных
  // описания фоток
  const descriptions = [
    'Опустевший пляж',
    'Пошли купаться!',
    'Прибрежные камни',
    'Наш милый фотограф',
    'Не ешь нас!',
    '...особенно если этот цвет - черный',
    'Клубничка',
    'Жажда - всё!',
    'Запускаем самолетики',
    'Что же мне обуть сегодня?',
    'Мы пройдем здесь',
    'Поехали, красавица, кататься!',
    'Легкий завтрак',
    'Котбургер',
    'Валенки, валенки эх да не подшиты стареньки',
    'Какое небо голубое',
    'Люди в черном',
    'Моя старушка',
    'Кто стучится дверь моя?',
    'Пальмы',
    'Я не голоден',
    'Секс в океане',
    'Крабик',
    'Are you ready?!',
    'Ох и трудная это работа...',
  ];

  // никнеймы комментаторов
  const nicknames = [
    'ШашлычОК ',
    'Злая печенька',
    'bublik ',
    'восставший из Зада',
    'Бегущий по граблям',
    'Я_ВеСеЛыЙ_ТаРаКаН',
    'Ангел-Предохранитель ',
    'Хубрик',
    'Облачко ',
    'Телепузик',
  ];

  // тексты комментариев
  const messages = [
    'Всё отлично!',
    'В целом всё неплохо. Но не всё.',
    'Когда вы делаете фотографию, хорошо бы убирать палец из кадра. В конце концов это просто непрофессионально.',
    'Моя бабушка случайно чихнула с фотоаппаратом в руках и у неё получилась фотография лучше.',
    'Лица у людей на фотке перекошены, как будто их избивают. Как можно было поймать такой неудачный момент?!',
    'Я поскользнулся на банановой кожуре и уронил фотоаппарат на кота и у меня получилась фотография лучше.',
    'Да это фоташоп!!!!!!!!',
    'Мега фото! Просто обалдеть. Как вам так удалось?',
  ];

  window.data = {
    randomInteger: randomInteger,
    random: random,
    randomPictureTemplate: randomPictureTemplate,
    descriptions: descriptions,
    nicknames: nicknames,
    messages: messages,
    imagesBlock: imagesBlock,
    randomImg: randomImg,
    pictureComments: pictureComments,
    pictureLikes: pictureLikes,
    numOfPictures: numOfPictures,
    numOfComments: numOfComments,
    amountOfObj: amountOfObj,
  }
})();
