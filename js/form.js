'use strict';

(function () {
  window.preview.likesCount.textContent = window.auxData.moke[0].likes; // показываю количество лайков
  window.preview.commentsCount.textContent = window.auxData.moke[0].comments.count; // показываю общее количество комментов
  window.preview.socialPicture[0].src = window.auxData.moke[0].comments.avatar; // показываю аватарки ->
  window.preview.socialPicture[1].src = window.auxData.moke[1].comments.avatar; // -> комментаторов
  window.preview.socialPicture[0].alt = window.auxData.moke[0].comments.name; // имена ->
  window.preview.socialPicture[1].alt = window.auxData.moke[1].comments.name; // -> комментаторов
  window.preview.socialText[0].textContent = window.auxData.moke[0].comments.message; // комментарии
  window.preview.socialText[1].textContent = window.auxData.moke[1].comments.message; // комментарии
  window.preview.socialCaption.textContent = window.auxData.moke[20].description; // показываю описание фотографии

  // добавляю класс modal-open, чтобы контейнер с фотографиями позади не прокручивался при скролле.
  document.body.classList.add('modal-open');

  // ================================================
  // Загрузка изображения и показ формы редактирования

  const upload = document.querySelector('.img-upload'); // форма загрузки фотографий
  const uploadFile = upload.querySelector('#upload-file'); // контрол загрузки файла
  const imgUploadOverlay = upload.querySelector('.img-upload__overlay'); // форма редактирования изображения
  const uploadCansel = upload.querySelector('#upload-cancel'); // кнопка закрытия формы

  // обработчик по "эскейпу" внутри открытой формы
  const onUploadEsc = function (evt) {
    if (evt.key === 'Escape') {
      imgUploadOverlay.classList.add('hidden');
    }
  }

  // обработчик по "энтеру" на кнопке выхода из формы
  const onUploadCanselEnter = function (evt) {
    if (evt.key === 'Enter') {
      imgUploadOverlay.classList.add('hidden');
    }
  }

  // обработчик по изменению в поле добавления файла
  const onUploadChange = function () {
    imgUploadOverlay.classList.remove('hidden');
    uploadCansel.addEventListener('click', function () {
      imgUploadOverlay.classList.add('hidden');
    })
    uploadCansel.addEventListener('keydown', onUploadCanselEnter);
    document.addEventListener('keydown', onUploadEsc);
  }

  // регистрирую собатие изменения в поле добавления файла
  uploadFile.addEventListener('change', onUploadChange);

  // ================================================
  // Применение эффекта для изображения и редактирование размера изображения

  const effectLevelPin = upload.querySelector('.effect-level__pin');
  const effectLevelLine = upload.querySelector('.effect-level__line');

  // обработчик отпускания мыши на ползунке
  const onPinMouseup = function () {
    let rez = Math.round((effectLevelPin.offsetLeft / effectLevelLine.offsetWidth) * 100, -3);
    return rez;
  }

  effectLevelPin.addEventListener('mouseup', onPinMouseup);

  window.form = {
    upload: upload,
  }
})()

// ===============================================
// код предыдушего разработчика
// import { getPreviewPhotos } from './picture.js';
// import { getBigPicture } from './big-picture.js';
// import { setFormSubmit, showMessageDataLoadingError } from './sending-form.js'
// import './validation-form.js';
// import { getData } from './api.js';
// import { filtersContainer, getPictureForFilters } from './filters.js';

// getData((pictures) => {
//   getPreviewPhotos(pictures);
//   getBigPicture(pictures);
//   getPictureForFilters(pictures);
//   filtersContainer.classList.remove('img-filters--inactive');
// },
// (error) => showMessageDataLoadingError(error),
// );

// setFormSubmit();
