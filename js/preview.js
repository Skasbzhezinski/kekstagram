'use strict';

(function () {
  const bigPicture = document.querySelector('.big-picture'); // блок полноразмерных фотографий
  const bigPictureImg = bigPicture.querySelector('.big-picture__img img'); // полноразмерное фото
  const likesCount = bigPicture.querySelector('.likes-count'); // span с кол-вом лайков
  const commentsCount = bigPicture.querySelector('.comments-count'); // span с кол-вом комментов
  const socialComments = bigPicture.querySelector('.social__comments'); // список с комментами
  const socialPicture = socialComments.querySelectorAll('.social__picture'); // аватарки
  const socialText = socialComments.querySelectorAll('.social__text'); // комменты
  const socialCaption = bigPicture.querySelector('.social__caption'); // описание фото

  bigPictureImg.src = 'img/logo-background-' + window.data.randomInteger(1, 3) + '.jpg';

  // ================================================
  // Личный проект: доверяй, но проверяй (часть 2)

  const picturesContainer = document.querySelector('.pictures');
  const bigPictureCancel = bigPicture.querySelector('.big-picture__cancel'); // кнопка выхода
  // находим фотки по которым надо кликать для вызова полноразмерного изображения
  const pictures = picturesContainer.querySelectorAll('.picture');

  // функция показа полноэкранного изображения по клику
  const showFullScreen = function () {
    for (let i = 0; i < pictures.length; i++) {
      let currentPicture = pictures[i];

      //обработчик по клику на currentPicture
      currentPicture.addEventListener('click', function (evt) {
        evt.preventDefault();
        bigPicture.querySelector('.big-picture__img img').src = currentPicture.firstElementChild.src;
        bigPicture.classList.remove('hidden');
        bigPictureCancel.addEventListener('click', function () {
          bigPicture.classList.add('hidden');
        })

        // обработчик по энтеру на кнопке выхода "X"
        bigPictureCancel.addEventListener('keydown', function (evt) {
          if (evt.key === 'Enter') {
            bigPicture.classList.add('hidden');
          }
        })

        // обработчик по энтеру на currentPicture
        currentPicture.addEventListener('keydown', function (evt) {
          if (evt.key === 'Enter') {
            bigPicture.querySelector('.big-picture__img img').src = currentPicture.firstElementChild.src;
            bigPicture.classList.remove('hidden');
          }
        })

        // обработчик по эскейпу
        window.addEventListener('keydown', function (evt) {
          if (evt.key === 'Escape') {
            bigPicture.classList.add('hidden');
          }
        })
      });
    }
  }

  showFullScreen();

  window.preview = {
    likesCount: likesCount,
    commentsCount: commentsCount,
    socialPicture: socialPicture,
    socialText: socialText,
    socialCaption: socialCaption,
    picturesContainer: picturesContainer,
  }
})()

